// MongoDB Query Operators & Fields Projection

db.users.updateOne(
    {_id: ObjectId("61fbcaf023f433eefcb79b44")},
    {
        $set: {skills: []}
    }
)

db.users.updateOne(
    {_id: ObjectId("61fbcaf023f433eefcb79b44")},
    {
        $push: {
            skills: "HTML"
        }
    }
)

db.users.updateOne(
    {_id: ObjectId("61fbcaf023f433eefcb79b44")},
    {
        $push: {
            skills: {
                $each: ["CSS", "JavaScript"]
            }
        }
    }
)

db.users.updateOne(
    {_id: ObjectId("61fbcaf023f433eefcb79b44")},
    {
        $unset: {
            skills: ""
        }
    }
)

// Mini-Activity
db.users.insertMany([
    {
        name: "Dan", age: 24, isActive: true,
        skills: ["HTML", "CSS", "Bootstrap"]
    },
    {
        name: "Orwa", age: 22, isActive: true,
        skills: ["HTML", "CSS", "JavaScript"]
    },
    {
        name: "AmerECE", age: 23, isActive: true,
        skills: ["HTML", "MERN"]
    },
    {
        name: "Ala", age: 25, isActive: true,
        skills: ["HTML", "PHP", "MySQL"]
    },
    {
        name: "Belal", age: 27, isActive: true,
        skills: ["JavaScript", "Python"]
    }
])

// Search for users that know either JavaScript or MERN
db.users.find({
    skills: {
        $in: ["JavaScript", "MERN"]
    }
})

// Search for users that know HTML and CSS
db.users.find({
    skills: {
        $all: ["HTML", "CSS"]
    }
})

db.listingsAndReviews.find(
    {
        property_type: "Condominium",
        minimum_nights: "2",
        bedrooms: 1,
        accommodates: 2
    },
    {
        name: 1,
        property_type: 1,
        minimum_nights: 1,
        bedrooms: 1,
        accommodates: 1
    }
)
.limit(5)

// Show listings that have price of less than 100, fee for additional heads that is less than 20, and are in the country Portugal
db.listingsAndReviews.find(
    {
        price: {
            $lt: 100
        },
        extra_people: {
            $lt: 20
        },
        "address.country": "Portugal"
    },
    {
        name: 1,
        price: 1,
        extra_people: 1,
        "address.country": 1
    }
)
.limit(5)

// Search for the first 5 listing by name in ascending order that have a price less than 100.
db.listingsAndReviews.find(
    {
        price: {
            $lt: 100
        }
    },
    {
        name: 1,
        price: 1,
    }
)
.limit(5)
.sort({
    name: 1
})
// Use -1 for descending order

// /* 1 */
// {
//     "_id" : "11012484",
//     "name" : "",
//     "price" : NumberDecimal("40.00")
// }

// /* 2 */
// {
//     "_id" : "22210558",
//     "name" : "\"Hôtel\" room Frontenac metro, private & furnished",
//     "price" : NumberDecimal("40.00")
// }

// /* 3 */
// {
//     "_id" : "21568012",
//     "name" : "$29 per night, near city,free wifi, own room,WOW!!",
//     "price" : NumberDecimal("45.00")
// }

// /* 4 */
// {
//     "_id" : "20358824",
//     "name" : "$45ANYCCozy Room with curtain NearJ,G, and M train",
//     "price" : NumberDecimal("45.00")
// }

// /* 5 */
// {
//     "_id" : "21675318",
//     "name" : "( ͡° ͜ʖ ͡°) Baaasic Spaaace (ฅ´ω`ฅ)",
//     "price" : NumberDecimal("79.00")
// }

// increase the price of all listings by 10
db.listingsAndReviews.updateMany(
    {
        price: {
            $lt: 100
        }
    },
    {
        $inc: {
            price: -10
        }
    }
)

// delete listings with any one of the following conditions:
// review scores accuracy <=75
// review scores cleanliness <= 80
// review scores checkin <= 70
// review scores communication <= 70
// review scores location <= 75
// review scores value <= 80

db.listingsAndReviews.deleteMany(
{
    $or:[ 
        {"review_scores.review_scores_accuracy": {$lte: 75}},
        {"review_scores.review_scores_cleanliness": {$lte: 80}},
        {"review_scores.review_scores_checkin": {$lte: 70}},
        {"review_scores.review_scores_communication": {$lte: 70}},
        {"review_scores.review_scores_location": {$lte: 75}},
        {"review_scores.review_scores_value": {$lte: 80}}
    ]
}
)